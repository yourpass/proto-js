// @generated by protoc-gen-es v1.10.0 with parameter "target=js+dts"
// @generated from file yourpass/protobuf/variable_image_id.proto (package yourpass.protobuf, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import { proto3 } from "@bufbuild/protobuf";

/**
 * @generated from message yourpass.protobuf.VariableImageId
 */
export const VariableImageId = /*@__PURE__*/ proto3.makeMessageType(
  "yourpass.protobuf.VariableImageId",
  () => [
    { no: 1, name: "fallback_value", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 2, name: "variable_name", kind: "scalar", T: 9 /* ScalarType.STRING */ },
  ],
);

