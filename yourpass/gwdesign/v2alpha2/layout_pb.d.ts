// @generated by protoc-gen-es v1.10.0 with parameter "target=js+dts"
// @generated from file yourpass/gwdesign/v2alpha2/layout.proto (package yourpass.gwdesign.v2alpha2, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import type { BinaryReadOptions, FieldList, JsonReadOptions, JsonValue, PartialMessage, PlainMessage } from "@bufbuild/protobuf";
import { Message as Message$1, proto3 } from "@bufbuild/protobuf";
import type { VariableColor } from "../../protobuf/variable_color_pb.js";
import type { TemplatedString } from "../../protobuf/templated_string_pb.js";
import type { VariableImageId } from "../../protobuf/variable_image_id_pb.js";
import type { LocalizableTemplatedString } from "../../protobuf/localizable_templated_string_pb.js";
import type { LocalizableString } from "../../protobuf/localizable_string_pb.js";

/**
 * @generated from enum yourpass.gwdesign.v2alpha2.HolderPolicy
 */
export declare enum HolderPolicy {
  /**
   * @generated from enum value: HOLDER_POLICY_UNSPECIFIED = 0;
   */
  UNSPECIFIED = 0,

  /**
   * @generated from enum value: HOLDER_POLICY_MULTIPLE_HOLDERS = 1;
   */
  MULTIPLE_HOLDERS = 1,

  /**
   * @generated from enum value: HOLDER_POLICY_ONE_USER_ALL_DEVICES = 2;
   */
  ONE_USER_ALL_DEVICES = 2,

  /**
   * @generated from enum value: HOLDER_POLICY_ONE_USER_ONE_DEVICE = 3;
   */
  ONE_USER_ONE_DEVICE = 3,
}

/**
 * @generated from enum yourpass.gwdesign.v2alpha2.BarcodeFormat
 */
export declare enum BarcodeFormat {
  /**
   * @generated from enum value: BARCODE_FORMAT_UNSPECIFIED = 0;
   */
  UNSPECIFIED = 0,

  /**
   * @generated from enum value: BARCODE_FORMAT_AZTEC = 1;
   */
  AZTEC = 1,

  /**
   * @generated from enum value: BARCODE_FORMAT_CODE_39 = 2;
   */
  CODE_39 = 2,

  /**
   * @generated from enum value: BARCODE_FORMAT_CODE_128 = 3;
   */
  CODE_128 = 3,

  /**
   * @generated from enum value: BARCODE_FORMAT_CODABAR = 4;
   */
  CODABAR = 4,

  /**
   * @generated from enum value: BARCODE_FORMAT_DATA_MATRIX = 5;
   */
  DATA_MATRIX = 5,

  /**
   * @generated from enum value: BARCODE_FORMAT_EAN_8 = 6;
   */
  EAN_8 = 6,

  /**
   * @generated from enum value: BARCODE_FORMAT_EAN_13 = 7;
   */
  EAN_13 = 7,

  /**
   * @generated from enum value: BARCODE_FORMAT_ITF_14 = 8;
   */
  ITF_14 = 8,

  /**
   * @generated from enum value: BARCODE_FORMAT_PDF_417 = 9;
   */
  PDF_417 = 9,

  /**
   * @generated from enum value: BARCODE_FORMAT_QR_CODE = 10;
   */
  QR_CODE = 10,

  /**
   * @generated from enum value: BARCODE_FORMAT_UPC_A = 11;
   */
  UPC_A = 11,

  /**
   * @generated from enum value: BARCODE_FORMAT_TEXT_ONLY = 12;
   */
  TEXT_ONLY = 12,
}

/**
 * @generated from enum yourpass.gwdesign.v2alpha2.GenericType
 */
export declare enum GenericType {
  /**
   * @generated from enum value: GENERIC_TYPE_UNSPECIFIED = 0;
   */
  UNSPECIFIED = 0,

  /**
   * @generated from enum value: GENERIC_TYPE_SEASON_PASS = 1;
   */
  SEASON_PASS = 1,

  /**
   * @generated from enum value: GENERIC_TYPE_UTILITY_BILLS = 2;
   */
  UTILITY_BILLS = 2,

  /**
   * @generated from enum value: GENERIC_TYPE_PARKING_PASS = 3;
   */
  PARKING_PASS = 3,

  /**
   * @generated from enum value: GENERIC_TYPE_VOUCHER = 4;
   */
  VOUCHER = 4,

  /**
   * @generated from enum value: GENERIC_TYPE_GYM_MEMBERSHIP = 5;
   */
  GYM_MEMBERSHIP = 5,

  /**
   * @generated from enum value: GENERIC_TYPE_LIBRARY_MEMBERSHIP = 6;
   */
  LIBRARY_MEMBERSHIP = 6,

  /**
   * @generated from enum value: GENERIC_TYPE_RESERVATIONS = 7;
   */
  RESERVATIONS = 7,

  /**
   * @generated from enum value: GENERIC_TYPE_AUTO_INSURANCE = 8;
   */
  AUTO_INSURANCE = 8,

  /**
   * @generated from enum value: GENERIC_TYPE_HOME_INSURANCE = 9;
   */
  HOME_INSURANCE = 9,

  /**
   * @generated from enum value: GENERIC_TYPE_ENTRY_TICKET = 10;
   */
  ENTRY_TICKET = 10,

  /**
   * @generated from enum value: GENERIC_TYPE_RECEIPT = 11;
   */
  RECEIPT = 11,

  /**
   * @generated from enum value: GENERIC_TYPE_OTHER = 12;
   */
  OTHER = 12,
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.Layout
 */
export declare class Layout extends Message$1<Layout> {
  /**
   * classTemplateInfo.cardTemplateOverride
   *
   * @generated from field: repeated yourpass.gwdesign.v2alpha2.Row card_rows = 1;
   */
  cardRows: Row[];

  /**
   * classTemplateInfo.detailsTemplateOverride
   *
   * @generated from field: repeated yourpass.gwdesign.v2alpha2.FieldValue pass_details = 2;
   */
  passDetails: FieldValue[];

  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.ImageModule logo_image = 3;
   */
  logoImage?: ImageModule;

  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.ImageModule logo_wide_image = 4;
   */
  logoWideImage?: ImageModule;

  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.ImageModule hero_image = 5;
   */
  heroImage?: ImageModule;

  /**
   * @generated from field: yourpass.protobuf.VariableColor background_color = 6;
   */
  backgroundColor?: VariableColor;

  /**
   * @generated from field: yourpass.protobuf.TemplatedString grouping_id = 7;
   */
  groupingId?: TemplatedString;

  /**
   * TODO securityAnimation
   * TODO applinkdata
   *
   * classTemplateInfo.cardBarcodeSectionDetails
   *
   * @generated from field: yourpass.gwdesign.v2alpha2.Barcode barcode = 8;
   */
  barcode?: Barcode;

  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.SmartTap smart_tap = 20;
   */
  smartTap?: SmartTap;

  /**
   * @generated from oneof yourpass.gwdesign.v2alpha2.Layout.vertical
   */
  vertical: {
    /**
     * @generated from field: yourpass.gwdesign.v2alpha2.VerticalGeneric generic_vertical = 9;
     */
    value: VerticalGeneric;
    case: "genericVertical";
  } | {
    /**
     * @generated from field: yourpass.gwdesign.v2alpha2.VerticalLoyalty loyalty_vertical = 10;
     */
    value: VerticalLoyalty;
    case: "loyaltyVertical";
  } | { case: undefined; value?: undefined };

  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.HolderPolicy holder_policy = 21;
   */
  holderPolicy: HolderPolicy;

  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.Message change_message = 99;
   */
  changeMessage?: Message;

  constructor(data?: PartialMessage<Layout>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.Layout";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): Layout;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): Layout;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): Layout;

  static equals(a: Layout | PlainMessage<Layout> | undefined, b: Layout | PlainMessage<Layout> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.Barcode
 */
export declare class Barcode extends Message$1<Barcode> {
  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.BarcodeFormat format = 1;
   */
  format: BarcodeFormat;

  /**
   * @generated from field: yourpass.protobuf.TemplatedString message = 2;
   */
  message?: TemplatedString;

  /**
   * @generated from field: yourpass.protobuf.TemplatedString alt_text = 3;
   */
  altText?: TemplatedString;

  constructor(data?: PartialMessage<Barcode>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.Barcode";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): Barcode;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): Barcode;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): Barcode;

  static equals(a: Barcode | PlainMessage<Barcode> | undefined, b: Barcode | PlainMessage<Barcode> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.SmartTap
 */
export declare class SmartTap extends Message$1<SmartTap> {
  /**
   * @generated from field: repeated int64 redemption_issuers = 1;
   */
  redemptionIssuers: bigint[];

  /**
   * @generated from field: yourpass.protobuf.TemplatedString redemption_value = 2;
   */
  redemptionValue?: TemplatedString;

  constructor(data?: PartialMessage<SmartTap>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.SmartTap";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): SmartTap;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): SmartTap;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): SmartTap;

  static equals(a: SmartTap | PlainMessage<SmartTap> | undefined, b: SmartTap | PlainMessage<SmartTap> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.Row
 */
export declare class Row extends Message$1<Row> {
  /**
   * maximum of 3 items in a row
   *
   * @generated from field: repeated yourpass.gwdesign.v2alpha2.FieldValue fields = 1;
   */
  fields: FieldValue[];

  constructor(data?: PartialMessage<Row>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.Row";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): Row;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): Row;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): Row;

  static equals(a: Row | PlainMessage<Row> | undefined, b: Row | PlainMessage<Row> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.FieldValue
 */
export declare class FieldValue extends Message$1<FieldValue> {
  /**
   * could be generated if empty?
   *
   * @generated from field: string key = 1;
   */
  key: string;

  /**
   * @generated from oneof yourpass.gwdesign.v2alpha2.FieldValue.reference
   */
  reference: {
    /**
     * @generated from field: yourpass.gwdesign.v2alpha2.StructuredRef structured_ref = 2;
     */
    value: StructuredRef;
    case: "structuredRef";
  } | {
    /**
     * @generated from field: yourpass.gwdesign.v2alpha2.ImageModule image_module = 3;
     */
    value: ImageModule;
    case: "imageModule";
  } | {
    /**
     * @generated from field: yourpass.gwdesign.v2alpha2.TextModule text_module = 4;
     */
    value: TextModule;
    case: "textModule";
  } | {
    /**
     * link_module_key ???;
     *
     * @generated from field: yourpass.gwdesign.v2alpha2.DateModule date_module = 5;
     */
    value: DateModule;
    case: "dateModule";
  } | { case: undefined; value?: undefined };

  constructor(data?: PartialMessage<FieldValue>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.FieldValue";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): FieldValue;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): FieldValue;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): FieldValue;

  static equals(a: FieldValue | PlainMessage<FieldValue> | undefined, b: FieldValue | PlainMessage<FieldValue> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.StructuredRef
 */
export declare class StructuredRef extends Message$1<StructuredRef> {
  /**
   * @generated from field: repeated string field_paths = 1;
   */
  fieldPaths: string[];

  constructor(data?: PartialMessage<StructuredRef>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.StructuredRef";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): StructuredRef;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): StructuredRef;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): StructuredRef;

  static equals(a: StructuredRef | PlainMessage<StructuredRef> | undefined, b: StructuredRef | PlainMessage<StructuredRef> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.ImageModule
 */
export declare class ImageModule extends Message$1<ImageModule> {
  /**
   * @generated from field: yourpass.protobuf.VariableImageId id = 1;
   */
  id?: VariableImageId;

  /**
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString alt_text = 2;
   */
  altText?: LocalizableTemplatedString;

  constructor(data?: PartialMessage<ImageModule>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.ImageModule";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): ImageModule;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): ImageModule;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): ImageModule;

  static equals(a: ImageModule | PlainMessage<ImageModule> | undefined, b: ImageModule | PlainMessage<ImageModule> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.TextModule
 */
export declare class TextModule extends Message$1<TextModule> {
  /**
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString header = 1;
   */
  header?: LocalizableTemplatedString;

  /**
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString body = 2;
   */
  body?: LocalizableTemplatedString;

  constructor(data?: PartialMessage<TextModule>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.TextModule";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): TextModule;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): TextModule;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): TextModule;

  static equals(a: TextModule | PlainMessage<TextModule> | undefined, b: TextModule | PlainMessage<TextModule> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.DateModule
 */
export declare class DateModule extends Message$1<DateModule> {
  /**
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString header = 1;
   */
  header?: LocalizableTemplatedString;

  /**
   * @generated from oneof yourpass.gwdesign.v2alpha2.DateModule.fallback
   */
  fallback: {
    /**
     * Datetime to use as a fallback, must be in RFC3339 format.
     *
     * @generated from field: string fallback_value = 2;
     */
    value: string;
    case: "fallbackValue";
  } | {
    /**
     * Arbitrary text to use as a fallback.
     *
     * @generated from field: yourpass.protobuf.LocalizableString fallback_text = 3;
     */
    value: LocalizableString;
    case: "fallbackText";
  } | { case: undefined; value?: undefined };

  /**
   * @generated from field: string variable_name = 4;
   */
  variableName: string;

  /**
   * @generated from field: yourpass.protobuf.LocalizableString format = 5;
   */
  format?: LocalizableString;

  constructor(data?: PartialMessage<DateModule>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.DateModule";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): DateModule;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): DateModule;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): DateModule;

  static equals(a: DateModule | PlainMessage<DateModule> | undefined, b: DateModule | PlainMessage<DateModule> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.VerticalGeneric
 */
export declare class VerticalGeneric extends Message$1<VerticalGeneric> {
  /**
   * -- object
   *
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString card_title = 1;
   */
  cardTitle?: LocalizableTemplatedString;

  /**
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString header = 2;
   */
  header?: LocalizableTemplatedString;

  /**
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString subheader = 4;
   */
  subheader?: LocalizableTemplatedString;

  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.GenericType generic_type = 3;
   */
  genericType: GenericType;

  constructor(data?: PartialMessage<VerticalGeneric>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.VerticalGeneric";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): VerticalGeneric;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): VerticalGeneric;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): VerticalGeneric;

  static equals(a: VerticalGeneric | PlainMessage<VerticalGeneric> | undefined, b: VerticalGeneric | PlainMessage<VerticalGeneric> | undefined): boolean;
}

/**
 * TextModule account_id = 1; // body is not localizable
 * TextModule account_name = 2; // body is not localizable
 * TextModule loyalty_points = 3; // body is not localizable
 * TextModule secondary_loyalty_points = 3; // body is not localizable
 *
 * @generated from message yourpass.gwdesign.v2alpha2.VerticalLoyalty
 */
export declare class VerticalLoyalty extends Message$1<VerticalLoyalty> {
  /**
   * -- class
   *
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString program_name = 1;
   */
  programName?: LocalizableTemplatedString;

  /**
   * @generated from field: yourpass.protobuf.LocalizableTemplatedString issuer_name = 2;
   */
  issuerName?: LocalizableTemplatedString;

  constructor(data?: PartialMessage<VerticalLoyalty>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.VerticalLoyalty";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): VerticalLoyalty;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): VerticalLoyalty;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): VerticalLoyalty;

  static equals(a: VerticalLoyalty | PlainMessage<VerticalLoyalty> | undefined, b: VerticalLoyalty | PlainMessage<VerticalLoyalty> | undefined): boolean;
}

/**
 * @generated from message yourpass.gwdesign.v2alpha2.Message
 */
export declare class Message extends Message$1<Message> {
  /**
   * @generated from field: yourpass.gwdesign.v2alpha2.TextModule value = 1;
   */
  value?: TextModule;

  constructor(data?: PartialMessage<Message>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.gwdesign.v2alpha2.Message";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): Message;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): Message;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): Message;

  static equals(a: Message | PlainMessage<Message> | undefined, b: Message | PlainMessage<Message> | undefined): boolean;
}

