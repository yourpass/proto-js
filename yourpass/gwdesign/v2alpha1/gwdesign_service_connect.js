// @generated by protoc-gen-connect-es v1.4.0 with parameter "target=js+dts"
// @generated from file yourpass/gwdesign/v2alpha1/gwdesign_service.proto (package yourpass.gwdesign.v2alpha1, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import { CountGwDesignsRequest, CountGwDesignsResponse, CreateGwDesignRequest, CreateGwDesignResponse, DeleteGwDesignRequest, DeleteGwDesignResponse, GetGwDesignRequest, GetGwDesignResponse, ListGwDesignsRequest, ListGwDesignsResponse, PropagateChangesToDevicesRequest, PropagateChangesToDevicesResponse, UpdateGwDesignRequest, UpdateGwDesignResponse } from "./gwdesign_service_pb.js";
import { MethodKind } from "@bufbuild/protobuf";

/**
 * @generated from service yourpass.gwdesign.v2alpha1.GwDesignService
 */
export const GwDesignService = {
  typeName: "yourpass.gwdesign.v2alpha1.GwDesignService",
  methods: {
    /**
     * @generated from rpc yourpass.gwdesign.v2alpha1.GwDesignService.CreateGwDesign
     */
    createGwDesign: {
      name: "CreateGwDesign",
      I: CreateGwDesignRequest,
      O: CreateGwDesignResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.gwdesign.v2alpha1.GwDesignService.GetGwDesign
     */
    getGwDesign: {
      name: "GetGwDesign",
      I: GetGwDesignRequest,
      O: GetGwDesignResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.gwdesign.v2alpha1.GwDesignService.UpdateGwDesign
     */
    updateGwDesign: {
      name: "UpdateGwDesign",
      I: UpdateGwDesignRequest,
      O: UpdateGwDesignResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.gwdesign.v2alpha1.GwDesignService.DeleteGwDesign
     */
    deleteGwDesign: {
      name: "DeleteGwDesign",
      I: DeleteGwDesignRequest,
      O: DeleteGwDesignResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.gwdesign.v2alpha1.GwDesignService.ListGwDesigns
     */
    listGwDesigns: {
      name: "ListGwDesigns",
      I: ListGwDesignsRequest,
      O: ListGwDesignsResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.gwdesign.v2alpha1.GwDesignService.CountGwDesigns
     */
    countGwDesigns: {
      name: "CountGwDesigns",
      I: CountGwDesignsRequest,
      O: CountGwDesignsResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.gwdesign.v2alpha1.GwDesignService.PropagateChangesToDevices
     */
    propagateChangesToDevices: {
      name: "PropagateChangesToDevices",
      I: PropagateChangesToDevicesRequest,
      O: PropagateChangesToDevicesResponse,
      kind: MethodKind.Unary,
    },
  }
};

