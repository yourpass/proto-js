// @generated by protoc-gen-es v1.10.0 with parameter "target=js+dts"
// @generated from file yourpass/pass/v2/pass.proto (package yourpass.pass.v2, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import type { BinaryReadOptions, FieldList, JsonReadOptions, JsonValue, PartialMessage, PlainMessage, Struct, Timestamp } from "@bufbuild/protobuf";
import { Message, proto3 } from "@bufbuild/protobuf";

/**
 * @generated from message yourpass.pass.v2.Pass
 */
export declare class Pass extends Message<Pass> {
  /**
   * @generated from field: string project_id = 1;
   */
  projectId: string;

  /**
   * @generated from field: string id = 2;
   */
  id: string;

  /**
   * @generated from field: string pass_group_id = 3;
   */
  passGroupId: string;

  /**
   * Preview page url.
   *
   * @generated from field: string url = 5;
   */
  url: string;

  /**
   * Unique pass values assigned to variables specified by the project.
   *
   * @generated from field: google.protobuf.Struct data = 4;
   */
  data?: Struct;

  /**
   * Pass is marked as expired in the wallet.
   *
   * @generated from field: bool voided = 6;
   */
  voided: boolean;

  /**
   * Defines a time pass should be marked as expired in the wallet.
   *
   * @generated from field: google.protobuf.Timestamp expiration_time = 7;
   */
  expirationTime?: Timestamp;

  /**
   * First time yourpass received an event about pass being added to the wallet.
   *
   * @generated from field: google.protobuf.Timestamp first_registration_time = 12;
   */
  firstRegistrationTime?: Timestamp;

  /**
   * Last time yourpass received an event about pass being added to the wallet.
   *
   * @generated from field: google.protobuf.Timestamp last_registration_time = 13;
   */
  lastRegistrationTime?: Timestamp;

  /**
   * First time yourpass received an event about pass being removed from the wallet.
   *
   * @generated from field: google.protobuf.Timestamp first_unregistration_time = 14;
   */
  firstUnregistrationTime?: Timestamp;

  /**
   * Last time yourpass received an event about pass being removed from the wallet.
   *
   * @generated from field: google.protobuf.Timestamp last_unregistration_time = 15;
   */
  lastUnregistrationTime?: Timestamp;

  /**
   * Number of active registrations
   *
   * @generated from field: int32 device_count = 11;
   */
  deviceCount: number;

  /**
   * @generated from field: google.protobuf.Timestamp create_time = 8;
   */
  createTime?: Timestamp;

  /**
   * @generated from field: google.protobuf.Timestamp update_time = 9;
   */
  updateTime?: Timestamp;

  /**
   * @generated from field: google.protobuf.Timestamp delete_time = 10;
   */
  deleteTime?: Timestamp;

  constructor(data?: PartialMessage<Pass>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.pass.v2.Pass";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): Pass;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): Pass;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): Pass;

  static equals(a: Pass | PlainMessage<Pass> | undefined, b: Pass | PlainMessage<Pass> | undefined): boolean;
}

/**
 * @generated from message yourpass.pass.v2.PassUpdate
 */
export declare class PassUpdate extends Message<PassUpdate> {
  /**
   * @generated from field: string pass_group_id = 1;
   */
  passGroupId: string;

  /**
   * Unique pass values assigned to variables specified by the project.
   * Validated against project data specification.
   *
   * @generated from field: google.protobuf.Struct data = 2;
   */
  data?: Struct;

  /**
   * Mark pass as expired in the wallet.
   *
   * @generated from field: bool voided = 3;
   */
  voided: boolean;

  /**
   * Defines a time pass should be marked as expired in the wallet.
   *
   * @generated from field: google.protobuf.Timestamp expiration_time = 4;
   */
  expirationTime?: Timestamp;

  constructor(data?: PartialMessage<PassUpdate>);

  static readonly runtime: typeof proto3;
  static readonly typeName = "yourpass.pass.v2.PassUpdate";
  static readonly fields: FieldList;

  static fromBinary(bytes: Uint8Array, options?: Partial<BinaryReadOptions>): PassUpdate;

  static fromJson(jsonValue: JsonValue, options?: Partial<JsonReadOptions>): PassUpdate;

  static fromJsonString(jsonString: string, options?: Partial<JsonReadOptions>): PassUpdate;

  static equals(a: PassUpdate | PlainMessage<PassUpdate> | undefined, b: PassUpdate | PlainMessage<PassUpdate> | undefined): boolean;
}

