// @generated by protoc-gen-connect-es v1.4.0 with parameter "target=js+dts"
// @generated from file yourpass/iam/v2/iam_service.proto (package yourpass.iam.v2, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import { ChangeViewerPasswordRequest, ChangeViewerPasswordResponse, CountProjectsRequest, CountProjectsResponse, CountUsersRequest, CountUsersResponse, CreateProjectRequest, CreateProjectResponse, GetProjectRequest, GetProjectResponse, GetUserRequest, GetUserResponse, GetViewerRequest, GetViewerResponse, ListAccessRightsRequest, ListAccessRightsResponse, ListProjectsRequest, ListProjectsResponse, ListUsersRequest, ListUsersResponse, RemoveAccessRightsRequest, RemoveAccessRightsResponse, SetAccessRightsRequest, SetAccessRightsResponse, UpdateDataSpecificationRequest, UpdateDataSpecificationResponse, UpdateProjectRequest, UpdateProjectResponse } from "./iam_service_pb.js";
import { MethodKind } from "@bufbuild/protobuf";

/**
 * @generated from service yourpass.iam.v2.IamService
 */
export declare const IamService: {
  readonly typeName: "yourpass.iam.v2.IamService",
  readonly methods: {
    /**
     * @generated from rpc yourpass.iam.v2.IamService.CreateProject
     */
    readonly createProject: {
      readonly name: "CreateProject",
      readonly I: typeof CreateProjectRequest,
      readonly O: typeof CreateProjectResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.GetProject
     */
    readonly getProject: {
      readonly name: "GetProject",
      readonly I: typeof GetProjectRequest,
      readonly O: typeof GetProjectResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.UpdateProject
     */
    readonly updateProject: {
      readonly name: "UpdateProject",
      readonly I: typeof UpdateProjectRequest,
      readonly O: typeof UpdateProjectResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.UpdateDataSpecification
     */
    readonly updateDataSpecification: {
      readonly name: "UpdateDataSpecification",
      readonly I: typeof UpdateDataSpecificationRequest,
      readonly O: typeof UpdateDataSpecificationResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.ListProjects
     */
    readonly listProjects: {
      readonly name: "ListProjects",
      readonly I: typeof ListProjectsRequest,
      readonly O: typeof ListProjectsResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.CountProjects
     */
    readonly countProjects: {
      readonly name: "CountProjects",
      readonly I: typeof CountProjectsRequest,
      readonly O: typeof CountProjectsResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.GetUser
     */
    readonly getUser: {
      readonly name: "GetUser",
      readonly I: typeof GetUserRequest,
      readonly O: typeof GetUserResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.ListUsers
     */
    readonly listUsers: {
      readonly name: "ListUsers",
      readonly I: typeof ListUsersRequest,
      readonly O: typeof ListUsersResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.CountUsers
     */
    readonly countUsers: {
      readonly name: "CountUsers",
      readonly I: typeof CountUsersRequest,
      readonly O: typeof CountUsersResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.ListAccessRights
     */
    readonly listAccessRights: {
      readonly name: "ListAccessRights",
      readonly I: typeof ListAccessRightsRequest,
      readonly O: typeof ListAccessRightsResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.SetAccessRights
     */
    readonly setAccessRights: {
      readonly name: "SetAccessRights",
      readonly I: typeof SetAccessRightsRequest,
      readonly O: typeof SetAccessRightsResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.RemoveAccessRights
     */
    readonly removeAccessRights: {
      readonly name: "RemoveAccessRights",
      readonly I: typeof RemoveAccessRightsRequest,
      readonly O: typeof RemoveAccessRightsResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.GetViewer
     */
    readonly getViewer: {
      readonly name: "GetViewer",
      readonly I: typeof GetViewerRequest,
      readonly O: typeof GetViewerResponse,
      readonly kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.ChangeViewerPassword
     */
    readonly changeViewerPassword: {
      readonly name: "ChangeViewerPassword",
      readonly I: typeof ChangeViewerPasswordRequest,
      readonly O: typeof ChangeViewerPasswordResponse,
      readonly kind: MethodKind.Unary,
    },
  }
};

