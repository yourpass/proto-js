// @generated by protoc-gen-es v1.10.0 with parameter "target=js+dts"
// @generated from file yourpass/iam/v2/iam.proto (package yourpass.iam.v2, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import { proto3, Timestamp } from "@bufbuild/protobuf";
import { LocalizableString } from "../../protobuf/localizable_string_pb.js";

/**
 * @generated from enum yourpass.iam.v2.VariableKind
 */
export const VariableKind = /*@__PURE__*/ proto3.makeEnum(
  "yourpass.iam.v2.VariableKind",
  [
    {no: 0, name: "VARIABLE_KIND_UNSPECIFIED", localName: "UNSPECIFIED"},
    {no: 1, name: "VARIABLE_KIND_IMAGE_ID", localName: "IMAGE_ID"},
    {no: 2, name: "VARIABLE_KIND_STRING", localName: "STRING"},
    {no: 3, name: "VARIABLE_KIND_NUMBER", localName: "NUMBER"},
    {no: 4, name: "VARIABLE_KIND_DATE", localName: "DATE"},
    {no: 5, name: "VARIABLE_KIND_COLOR", localName: "COLOR"},
  ],
);

/**
 * @generated from message yourpass.iam.v2.Project
 */
export const Project = /*@__PURE__*/ proto3.makeMessageType(
  "yourpass.iam.v2.Project",
  () => [
    { no: 1, name: "parent_id", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 2, name: "id", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 3, name: "display_name", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 4, name: "description", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 5, name: "annotations", kind: "map", K: 9 /* ScalarType.STRING */, V: {kind: "scalar", T: 9 /* ScalarType.STRING */} },
    { no: 6, name: "data_specification", kind: "message", T: DataSpecification },
    { no: 7, name: "create_time", kind: "message", T: Timestamp },
    { no: 8, name: "update_time", kind: "message", T: Timestamp },
    { no: 9, name: "delete_time", kind: "message", T: Timestamp },
  ],
);

/**
 * @generated from message yourpass.iam.v2.ProjectUpdate
 */
export const ProjectUpdate = /*@__PURE__*/ proto3.makeMessageType(
  "yourpass.iam.v2.ProjectUpdate",
  () => [
    { no: 1, name: "display_name", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 2, name: "description", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 3, name: "annotations", kind: "map", K: 9 /* ScalarType.STRING */, V: {kind: "scalar", T: 9 /* ScalarType.STRING */} },
  ],
);

/**
 * @generated from message yourpass.iam.v2.DataSpecification
 */
export const DataSpecification = /*@__PURE__*/ proto3.makeMessageType(
  "yourpass.iam.v2.DataSpecification",
  () => [
    { no: 1, name: "variables", kind: "map", K: 9 /* ScalarType.STRING */, V: {kind: "message", T: VariableSpec} },
  ],
);

/**
 * @generated from message yourpass.iam.v2.VariableSpec
 */
export const VariableSpec = /*@__PURE__*/ proto3.makeMessageType(
  "yourpass.iam.v2.VariableSpec",
  () => [
    { no: 1, name: "kind", kind: "enum", T: proto3.getEnumType(VariableKind) },
    { no: 2, name: "display_name", kind: "message", T: LocalizableString },
  ],
);

/**
 * @generated from message yourpass.iam.v2.User
 */
export const User = /*@__PURE__*/ proto3.makeMessageType(
  "yourpass.iam.v2.User",
  () => [
    { no: 1, name: "id", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 2, name: "email", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 3, name: "display_name", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 5, name: "create_time", kind: "message", T: Timestamp },
    { no: 6, name: "update_time", kind: "message", T: Timestamp },
    { no: 7, name: "delete_time", kind: "message", T: Timestamp },
  ],
);

/**
 * @generated from message yourpass.iam.v2.AccessRights
 */
export const AccessRights = /*@__PURE__*/ proto3.makeMessageType(
  "yourpass.iam.v2.AccessRights",
  () => [
    { no: 1, name: "project_id", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 2, name: "user_id", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 3, name: "user_email", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 4, name: "user_display_name", kind: "scalar", T: 9 /* ScalarType.STRING */ },
    { no: 5, name: "access_rights", kind: "scalar", T: 3 /* ScalarType.INT64 */ },
    { no: 6, name: "create_time", kind: "message", T: Timestamp },
    { no: 7, name: "update_time", kind: "message", T: Timestamp },
    { no: 8, name: "delete_time", kind: "message", T: Timestamp },
  ],
);

