// @generated by protoc-gen-connect-es v1.4.0 with parameter "target=js+dts"
// @generated from file yourpass/iam/v2/iam_service.proto (package yourpass.iam.v2, syntax proto3)
/* eslint-disable */
// @ts-nocheck

import { ChangeViewerPasswordRequest, ChangeViewerPasswordResponse, CountProjectsRequest, CountProjectsResponse, CountUsersRequest, CountUsersResponse, CreateProjectRequest, CreateProjectResponse, GetProjectRequest, GetProjectResponse, GetUserRequest, GetUserResponse, GetViewerRequest, GetViewerResponse, ListAccessRightsRequest, ListAccessRightsResponse, ListProjectsRequest, ListProjectsResponse, ListUsersRequest, ListUsersResponse, RemoveAccessRightsRequest, RemoveAccessRightsResponse, SetAccessRightsRequest, SetAccessRightsResponse, UpdateDataSpecificationRequest, UpdateDataSpecificationResponse, UpdateProjectRequest, UpdateProjectResponse } from "./iam_service_pb.js";
import { MethodKind } from "@bufbuild/protobuf";

/**
 * @generated from service yourpass.iam.v2.IamService
 */
export const IamService = {
  typeName: "yourpass.iam.v2.IamService",
  methods: {
    /**
     * @generated from rpc yourpass.iam.v2.IamService.CreateProject
     */
    createProject: {
      name: "CreateProject",
      I: CreateProjectRequest,
      O: CreateProjectResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.GetProject
     */
    getProject: {
      name: "GetProject",
      I: GetProjectRequest,
      O: GetProjectResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.UpdateProject
     */
    updateProject: {
      name: "UpdateProject",
      I: UpdateProjectRequest,
      O: UpdateProjectResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.UpdateDataSpecification
     */
    updateDataSpecification: {
      name: "UpdateDataSpecification",
      I: UpdateDataSpecificationRequest,
      O: UpdateDataSpecificationResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.ListProjects
     */
    listProjects: {
      name: "ListProjects",
      I: ListProjectsRequest,
      O: ListProjectsResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.CountProjects
     */
    countProjects: {
      name: "CountProjects",
      I: CountProjectsRequest,
      O: CountProjectsResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.GetUser
     */
    getUser: {
      name: "GetUser",
      I: GetUserRequest,
      O: GetUserResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.ListUsers
     */
    listUsers: {
      name: "ListUsers",
      I: ListUsersRequest,
      O: ListUsersResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.CountUsers
     */
    countUsers: {
      name: "CountUsers",
      I: CountUsersRequest,
      O: CountUsersResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.ListAccessRights
     */
    listAccessRights: {
      name: "ListAccessRights",
      I: ListAccessRightsRequest,
      O: ListAccessRightsResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.SetAccessRights
     */
    setAccessRights: {
      name: "SetAccessRights",
      I: SetAccessRightsRequest,
      O: SetAccessRightsResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.RemoveAccessRights
     */
    removeAccessRights: {
      name: "RemoveAccessRights",
      I: RemoveAccessRightsRequest,
      O: RemoveAccessRightsResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.GetViewer
     */
    getViewer: {
      name: "GetViewer",
      I: GetViewerRequest,
      O: GetViewerResponse,
      kind: MethodKind.Unary,
    },
    /**
     * @generated from rpc yourpass.iam.v2.IamService.ChangeViewerPassword
     */
    changeViewerPassword: {
      name: "ChangeViewerPassword",
      I: ChangeViewerPasswordRequest,
      O: ChangeViewerPasswordResponse,
      kind: MethodKind.Unary,
    },
  }
};

