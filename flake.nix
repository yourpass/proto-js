{
  inputs = {
    nix.url = "git+ssh://git@bitbucket.org/yourpass/nix";
  };

  outputs = { self, nix }: {
    formatter = nix.formatter;

    devShells = nix.lib.forAllSystems (pkgs: {
      default = pkgs.devshell.mkShell {
        name = "yp-proto";

        packages = [
          pkgs.nodejs_22
          pkgs.buf
        ];

        commands = [
          {
            name = "fix";
            help = "format & fix found issues";
            command = ''
              ${nix.lib.cd_root}
              nix fmt .
            '';
          }
          {
            name = "publish";
            help = "Publishes the code as an npm package";
            command = ''
              [[ $# -eq 0 ]] && echo "Please provide a tag as the first parameter" && exit 1
              TAG=$1

              OTP=''${2:-}

              ${nix.lib.cd_root}

              [[ $(git tag -l "$TAG") ]] && echo "Tag already exists" && exit 1

              npm version $TAG --allow-same-version=true --git-tag-version=false
              git add package.json
              git commit -m "Update package to version $TAG"
              git tag -a $TAG -m ""
              [[ $OTP == "" ]] && npm publish --access public
              [[ $OTP != "" ]] && npm publish --access public --otp $OTP

              echo "Please push the new git commits and tag to the remote"
            '';
          }
        ];
      };
    });
  };
}
